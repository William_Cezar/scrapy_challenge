import http
import sys
from concurrent.futures import ThreadPoolExecutor

from tools import Website

def main(url):
    '''
    Parses the website located at the given URL and prints
    its phone numbers and logo path.

    Args:
        url (str): The URL of the website to parse.

    Returns:
        None
    '''
    website_infos, status = Website(url).parse_infos()
    print(website_infos)
    if status >= 400:
        print(f'{status} - {http.client.responses[status]}')

if __name__ == '__main__':
    # Get a list of URLs from standard input
    urls = filter(bool, map(str.strip, sys.stdin))
    # Use a thread pool to parse the websites concurrently
    with ThreadPoolExecutor() as executor:
        executor.map(main, urls)


