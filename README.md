

## Introduction

This application accepts multiple URLs as input and scrapes them to extract phone numbers and logos. 
The output is a JSON-like object that includes the website, logo path, and all the phone numbers that were found.

## Install process

To execute the application, begin by running the following command:

```
docker build -t logo_and_phones_extractor .
```

Then, the program can be run by providing standard inputs to the application, using the following command:

```
cat websites.txt | docker run -i logo_and_phones_extractor
```

Alternatively, the program can be run locally using the following steps:

```
pip install -r requirements.txt
cat websites.txt | python -m main
```

Please note that it is advisable to create a virtual environment before running the program in this manner.