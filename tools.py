import string
import urllib
from dataclasses import dataclass
from typing import List, Tuple

import phonenumbers
import requests
from bs4 import BeautifulSoup


class Website:
    def __init__(self, url: str):
        # Initializes a Website object with a given URL.
        # Sets the object's URL, response, HTML source, phones, and logo attributes to None or an empty list.
        self.url = url
        self.response = None
        self.html_source = None
        self.phones = []
        self.logo = ''

    def get_html(self) -> bool:
        # Sends an HTTP request to the website's URL and stores the response.
        # If successful, sets the object's HTML source attribute to the response text.
        # Returns True if the request is successful, False otherwise.
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.53 Safari/537.36'}
        try:
            self.response = requests.get(self.url, headers=headers)
            self.html_source = self.response.text
            return True
        except Exception as e:
            print(f'Error with url {self.url}: {e}')
            return False

    def clean_phone(self, phone_number: str) -> str:
        # Takes a phone number as a string and removes all characters that are not digits, parentheses, or plus sign.
        # Returns the cleaned phone number as a string.
        allowed_characters = string.digits + '()+'
        return ''.join([char if char in allowed_characters else ' ' for char in phone_number])

    def format_phones(self, phones: List[str]) -> List[str]:
        # Takes a list of phone numbers and cleans each number using clean_phone().
        # Returns a new list of cleaned phone numbers.
        return [self.clean_phone(phone_number) for phone_number in phones]

    def format_logo(self, logo_url: str) -> str:
        # Takes a logo URL and formats it to an absolute path.
        # Returns the formatted URL as a string.
        url_splitted = urllib.parse.urlsplit(self.url)
        if logo_url.startswith('//'):
            return f'{url_splitted.scheme}:{logo_url}'
        elif logo_url.startswith('/'):
            return f'{url_splitted.scheme}://{url_splitted.hostname}{logo_url}'
        else:
            return logo_url

    def find_phones(self) -> List[str]:
        # Uses the PhoneNumberMatcher() function from the phonenumbers module to find phone numbers in the object's HTML source.
        # Adds any phone numbers found to the object's phones attribute.
        phones = []
        for country in ["BR", "US", "GB"]:
            phones_match = phonenumbers.PhoneNumberMatcher(self.html_source, country)
            phones += [phone.raw_string for phone in phones_match]
        
        phones = list(set(phones))
        self.phones = phones

    def find_logo(self) -> bool:
        # Parses the object's HTML source using BeautifulSoup and finds all image tags.
        # Looks for image tags with "logo" in their src, class, or alt attributes and sets the object's logo attribute to the formatted URL if found.
        # Returns True if a logo is found, False otherwise.
        soup = BeautifulSoup(self.html_source, 'html.parser')
        imgs = soup.find_all('img')
        for img in imgs:
            if 'logo' in img['src'].lower():
                self.logo = self.format_logo(img['src'])
                return True
            if img.has_attr('class'):
                for desc_class in img['class']:
                    if 'logo' in desc_class.lower():
                        self.logo = self.format_logo(img['src'])
                        return True
            if img.has_attr('alt'):
                for desc_alt in img['alt']:
                    if 'logo' in desc_alt.lower():
                        self.logo = self.format_logo(img['src'])
                        return True
        self.logo = ''
        return False

    def parse_infos(self) -> Tuple[dict, int]:
        # Calls get_html() to send an HTTP request to the website's URL and store the response.
        # Calls find_logo() and find_phones() to parse the object's HTML source for logos and phone numbers.
        # Formats the object's phone numbers and logo URL using format_phones() and format_logo().
        # Returns a tuple containing a dictionary with the website's URL, formatted phone numbers, and formatted logo URL, and an HTTP status code.
        if not self.get_html():
            ret_val = {
                'website': self.url.strip(),
                'phones': [],
                'logo': ''
            }
            return ret_val, -1
        self.find_logo()
        self.find_phones()
        ret_val = {
            'website': self.url.strip(),
            'phones': self.format_phones(self.phones),
            'logo': self.format_logo(self.logo),
        }
        return ret_val, self.response.status_code
